#!/usr/bin/make -f

#
# Verbosity
#
# export DH_VERBOSE=1

#
# debhelper compatibility level
# (provided in compat file)

#
# DEB_BUILD_OPTIONS?
#

#
# Environement
#

# include /usr/share/quilt/quilt.make

SHELL=/bin/sh
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
TARGET=$(CURDIR)/debian/tmp


#
# Extract version information from autoconf meta-data
#
MAJOR=$(shell grep -m 1 XOTCL_MAJOR_VERSION configure.ac|cut -d= -f2)
MINOR=$(shell grep -m 1 XOTCL_MINOR_VERSION configure.ac|cut -d= -f2)
PATCHLEVEL=$(shell grep -m 1 XOTCL_RELEASE_LEVEL configure.ac|cut -d= -f2)
VERSION=$(MAJOR).$(MINOR)$(PATCHLEVEL)

#
# deployment targets
#

DEB=$(CURDIR)/debian
EXTENSION=$(DEB)/xotcl
# Debian Tcl/Tk policy paths
ARCH_DEPENDENT=$(EXTENSION)/usr/lib/tcltk
ARCH_INDEPENDENT=$(EXTENSION)/usr/share/tcltk
DEV=$(EXTENSION)-dev
DOC=$(EXTENSION)-doc
SHELLS=$(EXTENSION)-shells
DOCDIR=$(DOC)/usr/share/doc/xotcl-doc

#
# NOTE: --with-tcl=/usr/lib needed? -> is anyway resolved 
# to /usr/share/tcltk
# NOTE: Due to a (possible) licensing conflict, we exlcude, for now,
# the language binding for gdbm (http://www.gnu.org/software/gdbm/).
# gdbm comes under GPL 2+ which affects "derived work", in terms of
# libraries linking against it. Upstream might switch to a gdbm
# alternative in future releases:
# --with-gdbm=/usr/include,/usr/lib	

config.status: configure

	dh_testdir
	dh_quilt_patch
	./configure --prefix=/usr \
		--exec-prefix=/usr \
		$(shell dpkg-buildflags --export=configure) \
		--enable-shared \
		--enable-threads \
		--with-actiweb=yes \
		--with-xotclsh=yes \
		--with-xowish=yes \
		--includedir=/usr/include/xotcl \
		--with-tclinclude=/usr/include/tcl \
		--with-tcl=/usr/lib \
		--with-tk=/usr/lib \
		--mandir=/usr/share/man \
		--with-expat=sys \
		--host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE)

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp

# build-stamp: $(QUILT_STAMPFN) config.status
build-stamp: config.status
	dh_testdir

	$(MAKE)

	touch $@

# clean: unpatch
clean: 
	dh_testdir
	dh_testroot
	rm -f build-stamp 
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_quilt_unpatch
	find . -name '*.o' -type f -delete
	find . -name '*.so' -type f -delete
	dh_clean config.log config.status config.cache config.guess

install: build 
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	$(MAKE) DESTDIR=$(TARGET) XOTCLSH=xotclsh install
	# / / / / / / / / / / / / / / / / / / / / / / / / / / /
	# xotcl
	#install -d -m 755 -o root -g root $(EXTENSION)/usr/lib
	install -d -m 755 -o root -g root $(ARCH_DEPENDENT)/xotcl$(VERSION)
	install -d -m 755 -o root -g root $(ARCH_INDEPENDENT)
	@cp -R $(TARGET)/usr/lib/* $(ARCH_DEPENDENT)
	for d in $$(cat $(DEB)/xotcl.arch_independent); do \
		mv $(ARCH_DEPENDENT)/xotcl$(VERSION)/$$d $(ARCH_INDEPENDENT)/xotcl$(VERSION)-$$d; \
	done

	# `- fix build-related symlinks
	(cd $(EXTENSION)/usr/lib; \
	ln -sf ./tcltk/xotcl$(VERSION)/libxotcl$(VERSION).so;)
	@rm -f $(ARCH_DEPENDENT)/libxotcl$(VERSION).so

	# remove misplaced/ inappropriate files
	rm -f $(ARCH_DEPENDENT)/xotcl$(VERSION)/COPYRIGHT

	# xotcl-shells
	install -d -m 755 -o root -g root $(SHELLS)/usr/bin
	@cp -R $(TARGET)/usr/bin/* $(SHELLS)/usr/bin/

	# man pages go with shells package (as required
	# by Debian policy, section 12.1)
	install -d -m 755 -o root -g root $(SHELLS)/usr/share/man/man1
	cp -R $(TARGET)/usr/share/man/man1/* $(SHELLS)/usr/share/man/man1

	# xotcl-dev
	install -d -m 755 -o root -g root $(DEV)/usr/include/xotcl
	cp -R $(TARGET)/usr/include/xotcl/* $(DEV)/usr/include/xotcl

	# move and fix xotclConfig.sh
	install -d -m 755 -o root -g root $(DEV)/usr/share/tcltk/xotcl$(VERSION)
	mv $(ARCH_DEPENDENT)/xotclConfig.sh $(DEV)/usr/share/tcltk/xotcl$(VERSION)/
	perl -i -p \
	-e "s|^(XOTCL_BUILD_LIB_SPEC)=.*$$|\1='-L/usr/lib/tcltk/xotcl$(VERSION) -lxotcl$(VERSION)'|;" \
	-e "s|^(XOTCL_BUILD_STUB_LIB_SPEC)=.*$$|\1='-L/usr/lib/tcltk/xotcl$(VERSION) -lxotclstub$(VERSION)'|;" \
	-e "s|^(XOTCL_INCLUDE_DIR)=.*$$|\1='/usr/include/xotcl'|;" \
	-e "s|^(XOTCL_BUILD_STUB_LIB_PATH)=.*$$|\1='/usr/lib/tcltk/xotcl$(VERSION)/libxotclstub$(VERSION).a'|;" \
	-e "s|^(XOTCL_LIB_SPEC)=.*$$|\1='-L/usr/lib/tcltk/xotcl$(VERSION) -lxotcl$(VERSION)'|;" \
	-e "s|^(XOTCL_STUB_LIB_SPEC)=.*$$|\1='-L/usr/lib/tcltk/xotcl$(VERSION) -lxotclstub$(VERSION)'|;" \
	-e "s|^(XOTCL_STUB_LIB_PATH)=.*$$|\1='/usr/lib/tcltk/xotcl$(VERSION)/libxotclstub$(VERSION).a'|;" \
	$(DEV)/usr/share/tcltk/xotcl$(VERSION)/xotclConfig.sh

	# `- provide symlinks 
	install -d -m 755 -o root -g root $(DEV)/usr/lib/tcltk/xotcl$(VERSION)

	(cd $(DEV)/usr/lib; \
	ln -sf ../share/tcltk/xotcl$(VERSION)/xotclConfig.sh;)

	# (cd $(DEV)/usr/lib/tcltk/xotcl$(VERSION); \
	# ln -sf ../../share/tcltk/xotcl$(VERSION)/xotclConfig.sh;)

	# move XOTCL stub library
	mv $(ARCH_DEPENDENT)/xotcl$(VERSION)/*.a $(DEV)/usr/lib/tcltk/xotcl$(VERSION)/

	# xotcl-doc
	install -d -m 755 -o root -g root $(DOCDIR)/readings
	for f in $(CURDIR)/doc/*.pdf $(CURDIR)/doc/*.html $(CURDIR)/doc/*.css $(CURDIR)/doc/*.gif ; do \
		cp $$f $(DOCDIR)/readings/; \
	done	

	mv $(ARCH_DEPENDENT)/xotcl$(VERSION)/apps $(DOCDIR)/examples

	# remove misplaced/inappropriate files
	rm -f $(DOCDIR)/examples/COPYRIGHT

	# lintian fixes:
	# `- E: xotcl: missing-dep-for-interpreter tclsh8.4 => tcl8.4 
	# (./usr/lib/xotcl$(VERSION)/apps/utils/xotclsh)
	# `- clear from *.in alter (reported by linda)
	perl -i -p \
	-e "s|^(#!).*$$|\1/usr/bin/tclsh|;" \
	$(DOCDIR)/examples/utils/xotclsh
	rm -f $(DOCDIR)/examples/utils/xotclsh.in
	# `- W: xotcl: interpreter-not-absolute 
	# ./usr/lib/xotcl$(VERSION)/apps/utils/xowish #!@WISH_PROG@
	# `- clear from *.in alter (reported by linda)
	perl -i -p \
	-e "s|^(#!).*$$|\1/usr/bin/wish|;" \
	$(DOCDIR)/examples/utils/xowish
	rm -f $(DOCDIR)/examples/utils/xowish.in

	# fix lintian executable-not-elf-or-script warnings
	for f in $$(cat $(DEB)/xotcl-doc.executable-not-elf-or-script); do \
		chmod 644 $(DOCDIR)/examples/$$f; \
	done

	# fix lintian interpreter-not-absolute warnings
	for f in $$(cat $(DEB)/xotcl-doc.interpreter-not-absolute); do \
		perl -i -p \
		-e "s|^(#!).*$$|\1/usr/bin/xotclsh|;" \
		$(DOCDIR)/examples/$$f; \
	done

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir -i
	dh_testroot -i
	dh_installdocs -i
	dh_installchangelogs -i ChangeLog
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir -a
	dh_testroot -a
	dh_installchangelogs -a ChangeLog
	dh_installdocs -a
	dh_installexamples -a
	dh_installman -a
	dh_link -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	tcltk-depends
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

get-orig-source:
	wget -O xotcl_1.6.8.orig.tar.gz \
	     http://media.wu-wien.ac.at/download/xotcl-1.6.8.tar.gz


binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install get-orig-source
