xotcl (1.6.8-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 18:08:13 +0100

xotcl (1.6.8-4) unstable; urgency=medium

  * Team upload.
  * Remove the aolserver4-xotcl package since aolserver4 is to be removed from
    Debian.
  * Replace old VCS links by new Salsa based ones.
  * Bump the standards version to 4.1.3.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 13 Apr 2018 11:24:36 +0300

xotcl (1.6.8-3) unstable; urgency=low

  * Silence some lintian warnings
  * Support reproducible builds (Closes: #840155)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Tue, 24 Jan 2017 00:34:08 +0100

xotcl (1.6.8-2) unstable; urgency=low

  * Silence lintian warnings
  * Support reproducible builds (Closes: #797543)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Thu, 18 Aug 2016 14:08:37 +0200

xotcl (1.6.8-1) unstable; urgency=medium

  * New upstream release: 1.6.8 (see http://www.xotcl.org/)
  * Closes: #724816
  * Silence lintian warnings

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 28 Apr 2014 17:28:54 +0200

xotcl (1.6.7-2) unstable; urgency=low

  * Delete all .o and .so files in the clean target explicitly, to
    remove unwanted build artifacts in the 1.6.7 upstream
    release. All credits go to Sven Hoexter for providing this
    patch. (Closes: #651969)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Sun, 01 Jan 2012 15:36:21 +0100

xotcl (1.6.7-1) unstable; urgency=low

  * New upstream release: 1.6.7 (see http://www.xotcl.org/)
  * Checked against lintian v2.5.4: Removed deprecated reference to
    BSD license file
    (copyright-refers-to-deprecated-bsd-license-file), revamped
    debian/rules to lay out recommmended targets
    (debian-rules-missing-recommended-target), bumped source package
    to Standards version 3.9.2
  * Switched the debian source package to using quilt, rather than dpatch
  * Updated VCS locators, thx to Sven Hoexter for pointing this out!

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 28 Nov 2011 00:22:21 +0100

xotcl (1.6.6-1.1) unstable; urgency=low

   * Non-maintainer upload.
   * No changes, rebuilt for for aolserver4-core-4.5.1-1

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 14 Apr 2011 21:35:40 +0200

xotcl (1.6.6-1) unstable; urgency=medium

  * New upstream release: 1.6.6 (see http://www.xotcl.org/)
  * This upstream version supports --with-expat=sys, now used in debian/rules
  * Removed obsolete Makefile.in patch
  * Checked lintian sanity (v2.3.4)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Sun, 11 Apr 2010 19:31:04 +0200

xotcl (1.6.5-1.2) unstable; urgency=high

  * Non-maintainer upload by the Security Team
  * Fix CVE-2009-3720 in embedded Expat copy. xotcl isn't affected by
    CVE-2009-3560 (Closes: #560950)

  [ Stefan Sobernig ]
  * Incorporated NMU source patches on bundled expat properly through dpatch

 -- Moritz Muehlenhoff <jmm@debian.org>  Tue, 23 Feb 2010 21:51:50 +0100

xotcl (1.6.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add --with-expat=sys to configure (Closes: #560950)
  * Urgendcy medium due to RC bug fix

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Wed, 27 Jan 2010 10:02:03 +0100

xotcl (1.6.5-1) unstable; urgency=low

  * New upstream release: 1.6.5 (see http://www.xotcl.org/)
  * Checked lintian sanity (v2.2.18)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 16 Nov 2009 10:02:47 +0100

xotcl (1.6.4-1) unstable; urgency=low

  * New upstream release: 1.6.4 (see http://www.xotcl.org/)
  * Pitched to standards version 3.8.3
  * Checked linitian sanity (linitian v2.2.17)
  * Place the substvar injection (cat call) into the binary-indep
    target

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Fri, 23 Oct 2009 00:27:22 +0200

xotcl (1.6.3-2) unstable; urgency=low

  [ Stefan Sobernig ]
  * Adding debian/watch file
  * Making use of inheriting fields from xotcl source in debian/control
  * Removed overrides turned obsolete in xotcl source
  * Added details to the aolserver4-xotcl extended description
  * Added details to the xotcl-dev extended description
  * Checked lintian sanity (lintian v2.2.12)
  * Changed section field in aolserver4-xotcl to "httpd"
  * aolserver4-xotcl: Adding API/ABI coupling to aolserver4-core
    through substvar injection from aolserver4-dev (starting with
    4.5.1-5)

  [ Francesco Paolo Lovergine ]
  * Debhelper compatibility level pushed to 7.
  * Home page field moved to main section.
  * Substvar concat moved just before dh_gencontrol.

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Tue, 23 Jun 2009 16:40:22 +0200

xotcl (1.6.3-1) unstable; urgency=low

  * New upstream release: 1.6.3 (see http://www.xotcl.org/)
  * Debian policy 3.8.1 compliance
  * Checked lintian sanity (linitian v2.2.9)
  * Updated copyright dates in debian/copyright

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 06 Apr 2009 11:53:40 +0200

xotcl (1.6.2-1) experimental; urgency=low

  * New upstream release: 1.6.2 (see http://www.xotcl.org/)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Fri, 28 Nov 2008 17:30:12 +0100

xotcl (1.6.1-1) unstable; urgency=low

  * New upstream release: 1.6.1 (see http://www.xotcl.org/)
  * Removed redundant tk Depends entry for xotcl-shells
  * Updated copyright date in debian/copyright
  * Debian policy 3.8.0 compliance
  * Resolving Tcl dependencies by means of tcltk-depends
  * Fixing lintian warning on invalid doc-base section: doc-base-unknown-section 
  * Removed unneeded debian/xotcl.script-not-executable fixes
  * Removed obsolete lintian overrides (as of lintian 2.14.1)

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 30 Jun 2008 00:55:26 +0200

xotcl (1.6.0-1) unstable; urgency=low

  * Initial release
  * Closes: bug#467502

 -- Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>  Mon, 25 Feb 2008 00:55:26 +0200
